const express = require('express')
const useragent = require('useragent');
const app = express()

app.get('/', function (req, res) {
  var agent = useragent.parse(req.headers['user-agent']);
  var accep_lang = req.headers["accept-language"];
  
  
  var return_data = {
    "ip" : req.headers['x-forwarded-for'],
    "language" : accep_lang.substr(0, accep_lang.indexOf(',')),
    "software" : agent.os.toString()
  }
  res.send(return_data)
})


app.listen(process.env.PORT || 8080, function () {
  console.log('Example app listening on port XXXX!')
})
